using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Services.Interfaces;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly IVendaServices _vendaServices;
        public VendaController(IVendaServices vendaServices)
        {
            _vendaServices = vendaServices;
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<Venda>> PegaVendaPorID([FromRoute] Guid id){
            var venda = await _vendaServices.PegaVendaPorID(id);
            return Ok(venda);
        }
        
        [HttpGet]
        public async Task<ActionResult<Venda>> PegaTodasVendas(){
            var vendas = await _vendaServices.PegaTodasVendas();
            return Ok(vendas);
        }

        [HttpPost]
        public async Task<ActionResult<Venda>> AdicionaVenda([FromBody] Venda novaVenda){
            var venda = await _vendaServices.AdicionaVenda(novaVenda);
            return CreatedAtAction(nameof(PegaVendaPorID), new {id = venda.ID}, venda);
        }
        [HttpPatch("{id}/status/{novoStatus}")]
        public async Task<ActionResult<Venda>> AtualizaStatusProduto([FromRoute] Guid id, [FromRoute] VendaStatus novoStatus){
            var venda = await _vendaServices.AtualizaStatusDaVenda(id, novoStatus);
            return CreatedAtAction(nameof(PegaVendaPorID), new {id = venda.ID}, venda);
        }
    }
}