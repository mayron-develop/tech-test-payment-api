using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Services.Interfaces;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly IVendedorServices _vendedorService;
        public VendedorController(IVendedorServices vendedorService)
        {
            _vendedorService = vendedorService;
        }
        
        [HttpGet]
        public async Task<ActionResult<List<Vendedor>>> PegaTodosVendedores(){
            var vendedores = await _vendedorService.PegaTodosVendedores();
            return Ok(vendedores);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<List<Vendedor>>> PegaProID([FromRoute] Guid id){
            var vendedores = await _vendedorService.PegaVendedorPorId(id);
            return Ok(vendedores);
        }

        [HttpPost]
        public async Task<ActionResult<List<Vendedor>>> AdicionaVendedor([FromBody] Vendedor novoVendedor){
            var vendedor = await _vendedorService.AdicionaVendedor(novoVendedor);
            return Ok(vendedor);
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<Vendedor>> AtualizaVendedor([FromRoute] Guid id, [FromBody] Vendedor novoVendedor){
            var vendedor = await _vendedorService.AtualizaVendedor(id, novoVendedor);
            return CreatedAtAction(nameof(PegaProID), new {id = vendedor.ID}, vendedor);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletaVendedor(Guid id){
            var resultado = await _vendedorService.DeletaVendedor(id);
            if(!resultado){
                return BadRequest("Erro ao deletar vendedor.");
            }
            return Ok("Vendedor deletado com sucesso.");
        }
    }
}