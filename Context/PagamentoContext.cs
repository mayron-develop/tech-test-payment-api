using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class PagamentoContext: DbContext
    {
        public PagamentoContext(DbContextOptions opt): base(opt){ }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder){
            modelBuilder
                .Entity<Venda>()
                .Property(v => v.Status)
                .HasConversion(new EnumToStringConverter<VendaStatus>());

        }
    }
}