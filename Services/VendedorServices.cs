using tech_test_payment_api.Models;
using tech_test_payment_api.Repository.Interface;
using tech_test_payment_api.Services.Interfaces;
using tech_test_payment_api.Helpers;
namespace tech_test_payment_api.Services
{
    public class VendedorServices: IVendedorServices
    {
        private readonly IVendedorRepository _vendedorRepo;
        public VendedorServices(IVendedorRepository vendedorRepo)
        {
            _vendedorRepo = vendedorRepo;
        }
        private async Task<Vendedor> PegaPorCPF(string CPF){
            var vendedor = await _vendedorRepo.PegaPorCPFAsync(CPF);
            return vendedor;
        }
        public async Task<Vendedor> AdicionaVendedor(Vendedor novoVendedor)
        {
            #region CPF
            // formata cpf
            novoVendedor.CPF.FormataCPF();
            // valida cpf
            ValidaVendedor.ValidaCPF(novoVendedor.CPF);
            // verifica se esta sendo usado
            if(await PegaPorCPF(novoVendedor.CPF) is not null){
                throw new ArgumentException("Já existe um vendedor com esse CPF");
            }
            #endregion

            // adiciona vendedor
            var vendedorAdicionado = await _vendedorRepo.AdicionaAsync(novoVendedor);
            return vendedorAdicionado;
        }

        public async Task<Vendedor> AtualizaVendedor(Guid id, Vendedor novoVendedor)
        {
            #region ID
            // pega vendedor com id passado
            var vendedorComID = await _vendedorRepo.PegaPorIDAsync(id);
            // verifica se vendedor existe
            if(vendedorComID is null){
                throw new ArgumentException("O vendedor não pode ser atualizado. Ele não existe.");
            }
            #endregion

            #region CPF
            // formata cpf
            novoVendedor.CPF.FormataCPF();
            // valida cpf
            ValidaVendedor.ValidaCPF(novoVendedor.CPF);
            // verifica se foi atualizado o cpf
            if(vendedorComID.CPF != novoVendedor.CPF){
                // verifica se o cpf esta sendo usado
               if(await PegaPorCPF(novoVendedor.CPF) is not null){
                    throw new ArgumentException("Já existe um vendedor com esse CPF");
                } 
            }
            #endregion

            novoVendedor.ID = vendedorComID.ID;
            var vendedorAtualizado = await _vendedorRepo.AtualizaAsync(novoVendedor);

            return vendedorAtualizado;
        }

        public async Task<bool> DeletaVendedor(Guid id)
        {
            // pega vendedor pelo id passado
            var vendedor = await _vendedorRepo.PegaPorIDAsync(id);
            // verifica se vendedor existe
            if(vendedor is null){
                throw new Exception("Vendedor não encontrado.");
            }
            // deleta vendedor
            return await _vendedorRepo.DeletaAsync(vendedor);
        }

        public async Task<List<Vendedor>> PegaTodosVendedores()
        {
            var vendedores = await _vendedorRepo.PegaTodosAsync();
            if(vendedores is null){
                throw new NullReferenceException("Não existem vendedores ainda");
            }
            return vendedores;
        }

        public async Task<Vendedor> PegaVendedorPorId(Guid id)
        {
            // pega pelo id passado
            var vendedor = await _vendedorRepo.PegaPorIDAsync(id);
            // verifica se o vendedor existe
            if(vendedor is null){
                throw new HttpRequestException("Nenhum vendedor encontrado");
            }
            return vendedor;
        }
    }
}