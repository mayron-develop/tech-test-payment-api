using tech_test_payment_api.Models;

namespace tech_test_payment_api.Services.Interfaces
{
    public interface IVendaServices
    {
        public Task<Venda> PegaVendaPorID(Guid id);
        public Task<List<Venda>> PegaTodasVendas();
        public Task<Venda> AdicionaVenda(Venda venda);
        public Task<Venda> AtualizaStatusDaVenda(Guid id, VendaStatus novoStatus);
        public Task<bool> DeletaVenda(Guid id);
    }
}