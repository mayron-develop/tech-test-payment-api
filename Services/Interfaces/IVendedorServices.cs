using tech_test_payment_api.Models;

namespace tech_test_payment_api.Services.Interfaces
{
    public interface IVendedorServices
    {
        public Task<Vendedor> PegaVendedorPorId(Guid id);
        public Task<List<Vendedor>> PegaTodosVendedores();
        public Task<Vendedor> AdicionaVendedor(Vendedor novoVendedor);
        public Task<Vendedor> AtualizaVendedor(Guid id, Vendedor novoVendedor);
        public Task<bool> DeletaVendedor(Guid id);
    }
}