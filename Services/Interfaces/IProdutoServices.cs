using tech_test_payment_api.Models;

namespace tech_test_payment_api.Services.Interfaces
{
    public interface IProdutoServices
    {
        public Task<Produto> PegaProdutoPorID(Guid id);
        public Task<List<Produto>> PegaTodosProdutos();
        public Task<Produto> AdicionarProduto(Produto novoProduto);
        public Task<Produto> AtualizaProduto(Guid id, Produto novoProduto);
        public Task<bool> DeletaProduto(Guid id);
    }
}