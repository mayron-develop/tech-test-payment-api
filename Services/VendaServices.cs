using tech_test_payment_api.Models;
using tech_test_payment_api.Repository.Interface;
using tech_test_payment_api.Services.Interfaces;

namespace tech_test_payment_api.Services
{
    public class VendaServices : IVendaServices
    {
        private readonly IVendaRepository _vendaRepo;
        private readonly IVendedorRepository _vendedorRepo;

        public VendaServices(IVendaRepository vendaRepo, IVendedorRepository vendedorRepo)
        {
            _vendaRepo = vendaRepo;
            _vendedorRepo = vendedorRepo;
        }
        
        public async Task<List<Venda>> PegaTodasVendas()
        {
            // pega pelo id passado
            var vendas = await _vendaRepo.PegaTodasAsync();
            // verifica se o vendedor existe
            if(vendas is null){
                throw new NullReferenceException("Ainda não foram criadas vendas");
            }
            return vendas;
        }
        public async Task<Venda> PegaVendaPorID(Guid id)
        {
            // pega pelo id passado
            var venda = await _vendaRepo.PegaPorIDAsync(id);
            // verifica se o vendedor existe
            if(venda is null)
            {
                throw new ArgumentException("Nenhuma venda encontrado");
            }
            return venda;
        }
        public async Task<Venda> AdicionaVenda(Venda novaVenda)
        {
            // verifica id do vendedor
            if(novaVenda.VendedorID == Guid.Empty)
            {
                throw new ArgumentNullException("Você deve fornecer um vendedor valido");
            }
            var vendedor = await _vendedorRepo.PegaPorIDAsync(novaVenda.VendedorID);
            if(vendedor is null){
                throw new ArgumentException("O vendedor não existe.");
            }
            // se for enviado um status diferente na hora de criar a venda, ele sera trocado antes de adicionar
            if(novaVenda.Status != VendaStatus.AguardandoPagamento){
                novaVenda.Status = VendaStatus.AguardandoPagamento;
            }
            var venda = await _vendaRepo.AdicionaAsync(novaVenda);
            return venda;
        }
        public async Task<Venda> AtualizaStatusDaVenda(Guid id, VendaStatus novoStatus)
        {
            var venda = await _vendaRepo.PegaPorIDAsync(id);
            if(venda is null)
            {
                throw new ArgumentException("Não foi encontrada nenhuma venda.");
            }
            var statusAtual = venda.Status;
            switch(statusAtual){
                case VendaStatus.AguardandoPagamento:
                    if(novoStatus == VendaStatus.PagamentoAprovado || novoStatus == VendaStatus.Cancelada){
                        venda.Status = novoStatus;
                    }else{
                        throw new ArgumentException("Não foi possivel atualizar a venda, sequencia de passos incompativeis");
                    }
                    break;
                
                case VendaStatus.PagamentoAprovado:
                    if(novoStatus == VendaStatus.EnviadoParaTransportadora || novoStatus == VendaStatus.Cancelada){
                        venda.Status = novoStatus;
                    }else{
                        throw new ArgumentException("Não foi possivel atualizar a venda, sequencia de passos incompativeis");
                    }
                    break;
                
                case VendaStatus.EnviadoParaTransportadora:
                    if(novoStatus == VendaStatus.Entregue){
                        venda.Status = novoStatus;
                    }else{
                        throw new ArgumentException("Não foi possivel atualizar a venda, sequencia de passos incompativeis");
                    }
                    break;
                case VendaStatus.Cancelada:
                    throw new NotSupportedException("Não é possivel atualizar a status da venda, ela já foi cancelada");
                default:
                    throw new ArgumentException("Não foi possivel atualizar o status do seu time");
            }
            var vendaAtualizada = await _vendaRepo.AtualizaAsync(venda);
            
            return vendaAtualizada;
        }

        public async Task<bool> DeletaVenda(Guid id)
        {
            var venda = await _vendaRepo.PegaPorIDAsync(id);
            if(venda is null){
                throw new ArgumentException("Não foi encontrada nenhuma venda.");
            }
            return await _vendaRepo.DeletaAsync(venda);
        }

    }
}