using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Helpers
{
    public static class FormataVendedor
    {
        public static void FormataCPF(this string cpf){
            // tira espaços em branco
            cpf = cpf.Replace(" ", "");
            // tira pontos
            cpf = cpf.Replace(".", "");
            // tira hifen
            cpf = cpf.Replace("-", "");
        }
    }
}