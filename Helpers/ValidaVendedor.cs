using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Helpers
{
    public static class ValidaVendedor
    {
        public static void ValidaCPF(string cpf){
            // verifica tamanho 
            if(cpf.Length != 11) throw new ArgumentOutOfRangeException("CPF com tamanho invalido.");
            
            // verifica se contem somente numeros
            if(!long.TryParse(cpf, out var n)) throw new ArgumentException("O CPF deve conter somente números");
        } 
    }
}