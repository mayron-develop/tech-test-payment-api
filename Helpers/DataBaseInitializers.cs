using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Helpers
{
    public static class DataBaseInitializers
    {
        public static List<Vendedor> VendedoresIniciais(){
            return new List<Vendedor>{
                new Vendedor
                {
                    ID = Guid.NewGuid(),
                    CPF = "12345678901",
                    Email = "v@gmail.com",
                    Nome = "vendedor 1",
                    Telefone = "999999999"
                },
                new Vendedor
                {
                    ID = Guid.NewGuid(),
                    CPF = "12345678902",
                    Email = "v12@gmail.com",
                    Nome = "vendedor 4",
                    Telefone = "999999888"
                },
                new Vendedor
                {
                    ID = Guid.NewGuid(),
                    CPF = "12345678903",
                    Email = "v2q@gmail.com",
                    Nome = "vendedor 2",
                    Telefone = "999999111"
                }
            };
        }
    }
}