using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repository.Interface;

namespace tech_test_payment_api.Repository
{
    public class VendedorRepository: GenericoRepository<Vendedor>, IVendedorRepository
    {
        private readonly PagamentoContext _context;
        private readonly DbSet<Vendedor> _vendedor;
        public VendedorRepository(PagamentoContext context) : base(context)
        {
            _context = context;
            _vendedor = context.Vendedores;
        }
        public async Task<List<Vendedor>> PegaTodosAsync()
        {
            return await _vendedor
                .AsNoTracking()
                .ToListAsync();
        }
        public async Task<Vendedor> PegaPorCPFAsync(string CPF)
        {
            return await _vendedor
                .AsNoTracking()
                .FirstOrDefaultAsync(v => v.CPF.ToLower() == CPF.ToLower());
        }

        public async Task<Vendedor> PegaPorIDAsync(Guid id)
        {
            return await _vendedor
                .AsNoTracking()
                .FirstOrDefaultAsync(v => v.ID == id);
        }
    }
}