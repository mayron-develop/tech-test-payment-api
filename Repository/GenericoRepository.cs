using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.Repository.Interface;

namespace tech_test_payment_api.Repository
{
    public abstract class GenericoRepository<TEntity> : IGenericoRepository<TEntity> where TEntity : class
    {
        private readonly PagamentoContext _context;
        private readonly DbSet<TEntity> dbSet;
        public GenericoRepository(PagamentoContext context)
        {
            _context = context;
            dbSet = context.Set<TEntity>();
        }
        
        public async Task<TEntity> AdicionaAsync(TEntity entity)
        {
            dbSet.Add(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<TEntity> AtualizaAsync(TEntity entity)
        {
            dbSet.Update(entity);
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<bool> DeletaAsync(TEntity entity)
        {
            dbSet.Remove(entity);
            var resultado = await _context.SaveChangesAsync() > 0;
            return resultado;
        }

    }
}