using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Repository.Interface
{
    public interface IVendaRepository: IGenericoRepository<Venda>
    {
        public Task<Venda> PegaPorIDAsync(Guid id);
        public Task<List<Venda>> PegaTodasAsync();
    }
}