using tech_test_payment_api.Models;

namespace tech_test_payment_api.Repository.Interface
{
    public interface IVendedorRepository: IGenericoRepository<Vendedor>
    {
        public Task<Vendedor> PegaPorIDAsync(Guid id);
        public Task<List<Vendedor>> PegaTodosAsync();
        public Task<Vendedor> PegaPorCPFAsync(string CPF);
    }
}