namespace tech_test_payment_api.Repository.Interface
{
    public interface IGenericoRepository<T> where T: class
    {
        public Task<T> AdicionaAsync(T entity);
        public Task<T> AtualizaAsync(T entity);
        public Task<bool> DeletaAsync(T id);
    }
}