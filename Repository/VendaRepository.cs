using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repository.Interface;

namespace tech_test_payment_api.Repository
{
    public class VendaRepository: GenericoRepository<Venda>, IVendaRepository
    {
        private readonly PagamentoContext _context;
        private readonly DbSet<Venda> _venda;
        public VendaRepository(PagamentoContext context): base(context)
        {
            _context = context;
            _venda = context.Vendas;
        }

        public async Task<Venda> PegaPorIDAsync(Guid id)
        {
            return await _venda
                .AsNoTracking()
                .Include(v => v.Produtos)
                .Include(v => v.Vendedor)
                .FirstOrDefaultAsync(p => p.ID == id);
        }
        public async Task<List<Venda>> PegaTodasAsync()
        {
            return await _venda
                .AsNoTracking()
                .Include(v => v.Vendedor)
                .Include(v => v.Produtos)
                .ToListAsync();
        }
    }
}