using System.Net;

namespace tech_test_payment_api.Middlewares
{
    public class ExceptionMiddleware : IMiddleware
    {
        private readonly string ContentType = "application/json";
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try{
                await next(context);
            }
            catch(ArgumentException e){
                SendResponse(context, (int)HttpStatusCode.BadRequest, e.Message);
            }
            catch(NullReferenceException e){
                SendResponse(context, (int)HttpStatusCode.NotFound, e.Message);
            }
            catch(Exception e){
                SendResponse(context, (int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        private async void SendResponse(HttpContext context, int statusCode, string message)
        {
            context.Response.ContentType = ContentType;
            await context.Response.WriteAsJsonAsync(new {
                StatusCode = statusCode,
                Message = message
            });
        }
    }
}