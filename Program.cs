using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using tech_test_payment_api.Context;
using tech_test_payment_api.Helpers;
using tech_test_payment_api.Middlewares;
using tech_test_payment_api.Repository;
using tech_test_payment_api.Repository.Interface;
using tech_test_payment_api.Services;
using tech_test_payment_api.Services.Interfaces;


var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<PagamentoContext>(opt => opt.UseInMemoryDatabase("Pagamento"));
builder.Services.AddControllers().AddNewtonsoftJson(opt =>
    opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
);

builder.Services.AddScoped<IVendedorRepository, VendedorRepository>();
builder.Services.AddScoped<IVendedorServices, VendedorServices>();
builder.Services.AddScoped<IVendaRepository, VendaRepository>();
builder.Services.AddScoped<IVendaServices, VendaServices>();

builder.Services.AddScoped<ExceptionMiddleware>();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c => {
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "api-docs", Version = "v1" });
});
var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();

    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
    });
}
app.UseHttpsRedirection();

app.UseAuthorization();

app.UseMiddleware<ExceptionMiddleware>();

app.MapControllers();

/* Popula a tabela de vendedores */
#region Populate
var scope = app.Services.CreateScope();
var ctx = scope.ServiceProvider.GetRequiredService<PagamentoContext>();

ctx.Vendedores.AddRange(DataBaseInitializers.VendedoresIniciais());
ctx.SaveChanges();
#endregion

app.Run();
