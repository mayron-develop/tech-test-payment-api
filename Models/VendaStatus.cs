namespace tech_test_payment_api.Models
{
    public enum VendaStatus
    {
        PagamentoAprovado = 1,
        EnviadoParaTransportadora = 2,
        Entregue = 3,
        Cancelada = 4,
        AguardandoPagamento = 0
    }
}