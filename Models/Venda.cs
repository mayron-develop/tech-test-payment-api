using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        [Required(ErrorMessage = "Não é possivel realizar uma compra sem um vendedor.")]
        public Guid VendedorID { get; set; }
        public Vendedor Vendedor { get; set; }

        [Required(ErrorMessage = "É necessario ter algum produto para realizar a compra")]
        [MinLength(1, ErrorMessage = "É necessario ter algum produto para realizar a compra")]
        public ICollection<Produto> Produtos { get; set; }

        public VendaStatus Status { get; set; } = VendaStatus.AguardandoPagamento;
        
        public DateTime DataVenda { get; set; } = DateTime.Now;
    }
}