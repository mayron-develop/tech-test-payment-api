using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test_payment_api.Models
{
    public class Vendedor
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        
        public string Nome { get; set; }

        [Required(ErrorMessage = "É necessario informar o CPF do vendedor")]
        public string CPF { get; set; }

        [EmailAddress(ErrorMessage = "Esse não é um email valido")]
        public string Email { get; set; }

        [Phone(ErrorMessage = "Esse não é um número de telefone valido")]
        public string Telefone { get; set; }
    }
}